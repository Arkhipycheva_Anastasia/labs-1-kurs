#include <stdio.h>
#include "readling.h"
#include <stdlib.h>
#include <string.h>
#include <locale.h>

int main()
{
	BOOK *p = NULL;
	BOOK tmp;
	int N = 0, j, i, min;
	setlocale(LC_ALL, "rus");
	p = readling(&N);
	if (p == NULL)
	{
		printf("error");
		return 3;
	}

	for (j = 0; j<N; j++)
	{
		min = j;
		for (i = j + 1; i<N; i++)
		if (strcmp(p[i].fio, p[min].fio) > 0)
			min = i;
		tmp = p[min];
		p[min] = p[j];
		p[j] = tmp;
	}

	for (i = 0; i<N; i++)
		printf("%s%s%d\n", p[i].title, p[i].fio, p[i].year);

	return 0;
}
