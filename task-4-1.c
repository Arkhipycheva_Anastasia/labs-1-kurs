#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char **enterlines(int *schetlines);
int main()
{
	int *ptr;
	char **onesimb;
	int schetlines, i;
	char **str;
	ptr = &schetlines;
	str = enterlines(ptr);
	onesimb = (char**)calloc(schetlines, sizeof(char*));
	for (i = 0; i != schetlines; i++)
		onesimb[i] = str[i];
	for (i = schetlines - 1; i >= 0; i--)
		printf("%s\n", onesimb[i]);
	return 0;
}