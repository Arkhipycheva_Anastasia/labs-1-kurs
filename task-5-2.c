#include <stdio.h>
#include "readling.h"
#include <stdlib.h>
#include <locale.h>

int main()
{
	BOOK *p = NULL;
	int N = 0, i, MinYear, MinI, MaxYear, MaxI;
	setlocale(LC_ALL, "rus");
	p = readling(&N);
	MinYear = p[0].year;
	MinI = 0;
	MaxYear = p[1].year;
	MaxI = 1;
	for (i = 2; i<N; i++)
	{
		if (p[i].year>MaxYear)
		{
			MaxYear = p[i].year;
			MaxI = i;
		}
		else
		if (p[i].year<MinYear)
		{
			MinYear = p[i].year;
			MinI = i;
		}
	}
	printf("����� ������ ����� \n%s%s%d\n\n", p[MinI].title, p[MinI].fio, p[MinI].year);
	printf("����� ����� ����� \n%s%s%d\n\n", p[MaxI].title, p[MaxI].fio, p[MaxI].year);
	free(p);
	return;
}
